package test.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import model.ITweet;
import model.TwitterClient;

class TwitterClientTest {

	// Client to use mock dependency
	private TwitterClient client;

	@BeforeEach
	public void setUp() {
		this.client = new TwitterClient();
	}

	@Test
	public void testSendTweetOnce() {
		// Setup - Data
		// Create mock dependency for TwitterClient
		ITweet tweet = mock(ITweet.class);

		// Setup - Expectations
		// Tell Mockito what should happen when you getText() is called
		when(tweet.getText()).thenReturn("This is a tweet");

		// Exercise
		// The sendTweet method should call getText()
		this.client.sendTweet(tweet);

		// Verification
		// Verify that getText was in fact called by sendTweet()
		verify(tweet, atLeastOnce()).getText();
	}

	@Test
	public void testTwoTweets() {
		ITweet tweet1 = mock(ITweet.class);
		ITweet tweet2 = mock(ITweet.class);
		List<ITweet> tweets = new ArrayList<ITweet>();
		tweets.add(tweet1);
		tweets.add(tweet2);
		when(tweet1.getText()).thenReturn("One");
		when(tweet2.getText()).thenReturn("Two");
		this.client.sendSuccessiveTweets(tweets);
		verify(tweet1).getText();
		verify(tweet2).getText();
	}

	@Test
	public void testSpam() {
		ITweet spam = mock(ITweet.class);
		when(spam.getText()).thenReturn("I LIEK CHOCOLAT MILK");
		this.client.sendSpam(spam);
		verify(spam, atLeast(10)).getText();
		verify(spam, atMost(10)).getText();
	}

	@Test
	public void testRandomString() {
		TwitterClient spiedClient = spy(this.client);
		when(spiedClient.postRandomLetters()).thenReturn("ABC");
		String expected = "ABC";
		String actual = spiedClient.postRandomLetters();
		assertEquals(expected, actual);
	}

}
