/**
 * 
 */
package model;

/**
 * Interface that defines the behavior of tweets
 * @author Will Underwood
 *
 */
public interface ITweet {
	
	String getText();

}
