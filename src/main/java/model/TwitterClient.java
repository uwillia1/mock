/**
 * 
 */
package model;

import java.util.List;
import java.util.Random;

/**
 * Sends messages to Twitter
 * @author Will Underwood
 *
 */
public class TwitterClient {
	
	public void sendTweet(ITweet tweet) {
		String message = tweet.getText();
		// send message to Twitter
	}
	
	public void sendSuccessiveTweets(List<ITweet> tweets) {
		for (ITweet tweet : tweets) {
			String message = tweet.getText();
			// send message to Twitter
		}
	}
	
	public void sendSpam(ITweet tweet) {
		for (int i = 0; i < 10; i++) {
			String message = tweet.getText();
			// send message to Twitter
		}
	}
	
	public String postRandomLetters() {
		String randomString = "";
		Random random = new Random();
		for (int i = 0; i < 280; i++) {
			char character = (char) (random.nextInt(26) + 'A');
			randomString += character;
		}
		return randomString;
	}

}
